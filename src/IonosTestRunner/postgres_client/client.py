import logging
import os
import sys
from configparser import ConfigParser
from pprint import pprint as pr

import pandas as pd
from appdirs import user_data_dir
from sqlalchemy import (Column, ForeignKey, Integer, MetaData, String, Table,
                        create_engine, text)
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.schema import CreateColumn

from IonosTestRunner.exceptions import IonosTestRunnerException

_logger = logging.getLogger(__name__)


class Client(object):
    '''
    classdocs
    '''
    def __init__(self,
                 db_host="LOCALHOST"):
        '''
        Constructor
        '''
        self.db_host = db_host.upper()
        self._get_connection_string()
        self._connect()

    def _get_connection_string(self):

        BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
        # print("BASE_DIR: {}".format(BASE_DIR))
        parser = ConfigParser()

        ud = user_data_dir()
        # print('user data dir is {}'.format(ud))

        settingsfile = os.path.join(ud, 'IonosTestRunner', 'dbsettings.ini')

        # print("reading settings file {}".format(settingsfile))
        if not os.path.exists(settingsfile):
            msg = "Expected settings file at {}. File not Found, Exit!".format(settingsfile)
            print(msg)
            sys.exit(2)

        try:
            parser.read(settingsfile)
        except FileNotFoundError:
            _logger.error("Settings File {} could not be found".format(settingsfile))
            raise
        except IOError:
            _logger.error("Settings File {} could not be read".format(settingsfile))
            raise
        except Exception:
            _logger.error("Settings File {} could not be read for unknown reasons".format(settingsfile))
        finally:
            _logger.info("Finished reading settings from {}".format(settingsfile))

        _logger.debug("settingsfile is {}".format(settingsfile))

        if self.db_host not in parser._sections.keys():
            section_str = '|'.join(parser._sections.keys())
            print('[ERROR] host {} has no section in config, possible keys are {}'.format(self.db_host, section_str))
            self.close()
            raise IonosTestRunnerException(msg="Bad Database connection!!!")
            sys.exit(2)

        self._connecton_str = 'postgresql://{}:{}@{}:{}/{}'

        self.connection_config = {}
        self.connection_config['user'] = parser.get(self.db_host, 'user')
        self.connection_config['password'] = parser.get(self.db_host, 'password')
        self.connection_config['host'] = parser.get(self.db_host, 'host')
        self.connection_config['port'] = parser.get(self.db_host, 'port')
        self.connection_config['db'] = parser.get(self.db_host, 'db')

        # self.print_con()

        self._connecton_str = self._connecton_str.format(parser.get(self.db_host, 'user'),
                                                         parser.get(self.db_host, 'password'),
                                                         parser.get(self.db_host, 'host'),
                                                         parser.get(self.db_host, 'port'),
                                                         parser.get(self.db_host, 'db'))

        _logger.info("Will try to make connection using {}".format(self._connecton_str))

    def print_con(self):

        _logger.debug("using following configuration to connect to database: ")
        _logger.debug("using following configuration to connect to database: {}".format(self.connection_config))

    def _connect(self):

        self.con = create_engine(self._connecton_str, client_encoding='utf8')
        self.meta = MetaData(bind=self.con)
        # print(con,meta)
        self.connection = self.con.connect()
        self.Base = declarative_base()
        # self.session = sessionmaker()
        # self.session.configure(bind=self.con)
        # self.s = session()

        _logger.info("con: ".format(self.con))
        _logger.info("meta".format(self.meta))
        _logger.info("connection".format(self.connection))

    def get_colnames(self, tablename, schema_name='public'):
        """
        return the column names of a table in tablename as a list
        """
        qr0 = """
        SELECT *
        FROM information_schema.columns
        WHERE table_schema = '`schemaname`'
        AND table_name   = '`tablename`'
        """

        qrCols = qr0.replace('`tablename`', tablename).replace('`schemaname`', schema_name)
        crsrCols = self._run_query(qrCols)
        columns = crsrCols.fetchall()

        return [x[3] for x in columns]

    def get_schema(self):
        data = self._run_query("SELECT * FROM pg_catalog.pg_tables;")
        return data.fetchall()

    def show_schema(self):
        data = self.get_schema()
        print(data)

    def table_report(self):
        _ = [print(x, self.count_rows(x)) for x in self.table_names()]

    def count_rows(self, table_name, exact=True):
        """Count rows of a speficic table

        """

        if exact:
            qr0 = "SELECT COUNT(*) FROM TNAMEd65ca2e09d37448fabc2d9716afe15a5;"
            qr0 = qr0.replace('TNAMEd65ca2e09d37448fabc2d9716afe15a5', table_name)
            # TODO: catch error when counting tables without rows
        else:
            qr0 = """SELECT reltuples AS approximate_row_count
            FROM pg_class
            WHERE relname = 'TNAMEd65ca2e09d37448fabc2d9716afe15a5';"""
            qr0 = qr0.replace('TNAMEd65ca2e09d37448fabc2d9716afe15a5', table_name)

            # return  int(pg_client.count_rows('action_tmp_000001').fetchall()[0][0])
            # return int(pg_client.count_rows('action_tmp_000001').fetchall()[0][0])
        result0 = self._run_query(qr0)
        return result0.fetchall()[0][0]

    def get_df(self, table_name):

        if table_name not in self.get_table_names():
            msg = 'trying to retrieve a non existent table'
            _logger.debug(msg)
            # print(msg) # TODO: use logger to treat this
            return None
        else:
            return pd.read_sql(table_name, self.connection)

    def put_df(self,
               df,
               table_name,
               chunksize=None,
               if_exists='replace',
               index=False):
        """write a dataframe to the database

        :param df: pandas DataFrame object to be written
        :param table_name: table name in the database
        :param chunksize(optional int): if present, incremental writing is carried out.
            The chunksize value corresponds to the number of lines written at once.
        :returns: None

        """
        df.to_sql(table_name,
                  self.connection,
                  if_exists=if_exists,
                  index=index,
                  chunksize=chunksize)

    def _run_query(self, query):
        """ function to run a query. This can be in standard sql syntax or also in
        sqlalchemy

        :param query: query string
        :returns: result of the query - might need further processing
        :rtype: ??

        """

        if query is not None:
            result = self.connection.execute(query)
            return(result)
        else:
            _logger.info("running with an empty query")

    def close(self):
        """Close the connection

        :returns: None

        """

        self.connection.close()

    def show_tables(self):
        """print the list of tables in the databsae

        Usage Example:
            postgres_con.show_tables()

        :returns: None

        """
        table_names = self.get_table_names()
        for x in table_names:
            print(x)

    def get_table_names(self):
        """get table names
        :returns: tables in the database
        :rtype: list of strings
        """
        return self.con.table_names()

    def drop_table(self, table_name):
        """delete table in `table_name` from the database

        :param table_name: string containing an existing table in the db
        :returns: the run query
        :rtype: sqlalchemy.engine.result.ResultProxy

        """

        table_names = self.get_table_names()
        if table_name not in table_names:
            # raise IonosTestRunnerException(msg="table {} does not exist".format(table_name))
            print("drop table: Table does not exists!")
            return None
        else:
            drop_statement = 'DROP TABLE "'+table_name+'";'
            return self._run_query(drop_statement)

    def _create_table(self, sql_alchemy_table, force=False):
        """Create a table if it does not exist

        :param sql_alchemy_table: an instance of an sql alchemy table
        :returns: None
        :rtype:  None

        """

        if not isinstance(sql_alchemy_table, Table):
            msg = "table input is not an sqlalchemy sqlalchemy.sql.schema.Table"
            raise IonosTestRunnerException(msg=msg)

        if not force and sql_alchemy_table.name in self.get_table_names():
            msg = "table already exists"
            raise IonosTestRunnerException(msg=msg)

        self.meta.create_all(self.con)

    def set_primary_key(self, index_name, table_name):
        """set primary keys

        :param index_name: which variables should become pk?
        :param table_name:  in which table should the pk be set?
        :returns: result of the query
        :rtype: sqlalchemy.engine.result.ResultProxy

        """

        table_names = self.get_table_names()

        if table_name not in table_names:
            msg = "set pk: Table {} does not exists!".format(table_name)
            print(msg)
            return None

        colnames = self.get_colnames(table_name)
        if index_name not in colnames:
            msg = "index column {} specified does not exist in table!".format(index_name)
            return None

        qr0 = """
        ALTER TABLE `tablename`
        ALTER `idxidentifier` DROP DEFAULT,
        ADD PRIMARY KEY (`idxidentifier`);
        """

        qr = qr0.replace('`tablename`', table_name).replace('`idxidentifier`', index_name)
        res = self._run_query(qr)

        return res

    def get_pk_columns(self, table_name):
        """ get the primary keys in a table
        :param table_name:
        :returns: columns names containing primary keys
        :rtype: list (of strings)

        """

        qr0 = """
        SELECT column_name
        FROM information_schema.table_constraints
             JOIN information_schema.key_column_usage
                 USING (constraint_catalog, constraint_schema, constraint_name,
                        table_catalog, table_schema, table_name)
        WHERE constraint_type = 'PRIMARY KEY'
          AND (table_schema, table_name) = ('public', '`tablename`')
        ORDER BY ordinal_position;
        """

        qrgetpk = qr0.replace('`tablename`', table_name)
        ff = self._run_query(qrgetpk)
        dat = ff.fetchall()
        pks = [x[0] for x in dat]

        return pks


if __name__ == '__main__':
    # main()
    pass
