#  coding: utf-8
# !/usr/bin/env python
# fib 300000

import coloredlogs  # noqa
import os
import datetime
import logging
import queue
import threading
import unittest
from unittest import TextTestRunner
from pathlib import Path
import uuid
from io import StringIO  # noqa
from pprint import pprint as pr  # noqa

from sqlalchemy import MetaData

log = logging.getLogger(__name__)  # name of module
log.debug('__name__ of current module: {}'.format(__name__))

class TestCase():
    """ TestCase class.
    Class that has following datas
    - contains a unittest testcase
    - metadata about the testcase (creation modification dates, container in which run etc.)
    Responsibilities are to maintain the data in the database, and run the testcase
    """
    def __init__(self, *args, **kwargs):
        self.data = dict()
        self.data.update(dict(*args, **kwargs))
        self.thread = None

    def __setitem__(self, key, value):
        """ td['key'] = value
        """
        self.data[key] = value

    def __getitem__(self, key):
        return self.data[key]

    def update(self, *args, **kwargs):
        self.data.update(dict(*args, **kwargs))

    def set_thread(self, thread):
        self.thread = thread

    def set_case(self, case):
        self.case = case

    def init_db(self, conn):
        # write initial database entry
        metadata = MetaData(bind=conn)
        metadata.reflect()
        test_run = metadata.tables['test_run']
        stmt = test_run.insert().values(self.data)
        conn.execute(stmt)

    def run(self, pg_client):

        conn = pg_client.connection
        metadata = MetaData(bind=conn)
        metadata.reflect()
        test_run = metadata.tables['test_run']
        logfile = os.path.join(os.getcwd(), 'logs', self['environment'], str(self['created_at'])+'.log')
        self['started_at'] = datetime.datetime.utcnow()
        self['status'] = 'running'
        self['logs'] = logfile

        stmt = test_run.update().where(test_run.c.test == self['test']).values(self.data)
        result = conn.execute(stmt)
        # test_result = unittest.TextTestRunner(verbosity=2).run(self.suite)

        os.makedirs(Path(logfile).parent, exist_ok=True)
        outStream = open(logfile, "a", encoding="utf-8")
        # outStream = StringIO()
        self.case.set_logstream(outStream)
        suite = unittest.TestSuite()
        suite.addTest(self.case)
        test_runner = TextTestRunner(stream=outStream,
                                     verbosity=2)

        test_result = test_runner.run(suite)

        """ Log unittest assertion Error Trace """
        outStream.write("\n ---- Writing test result trace - if any -------------")
        if test_result.failures == []:
            result_tuple = ('- No Error Trace -', )
        else:
            result_tuple = test_result.failures[0]

        # test_result.failures = ('OK - No Error Trace')
        for out_string in result_tuple:
            outStream.write('\n' + str(out_string))
        outStream.write("\n ---- Trace Finished -------------")
        # outStream.write(test_result.failures)

        outStream.flush()
        outStream.close()
        self['finished_at'] = datetime.datetime.utcnow()
        self['status'] = 'finished'

        stmt = test_run.update().where(test_run.c.test == self['test']).values(self.data)
        result = conn.execute(stmt) # noqa

class ThreadInstance(threading.Thread):
    """ Derived Thread Class
    - Carries data needed to function,
    - mainly overloads the run method, acquiring a lock and releasing it
    - call the testcase run method
    """
    def __init__(self,
                 threadID,
                 name,
                 threadLock,
                 testcase,
                 pg_client):
        # TODO: clean up arg structure!
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.testcase = testcase
        self.threadLock = threadLock
        self.pg_client = pg_client
        log.debug("Thread created by Container {}. Thread Name {}".format(self.testcase.data['environment'],
                                                                          self.name))

    def run(self):
        self.threadLock.acquire()
        log.debug("++Container {}: acquired lock for test case {}".format(self.testcase.data['environment'],
                                                                          self.testcase.data['test']))
        # log.debug("Data: \n {}".format(self.testcase.data['environment']))
        self._non_threaded_run()

        self.threadLock.release()
        log.debug("--Container {}: released lock for test case {}".format(self.testcase.data['environment'],
                                                                          self.testcase.data['test']))

    def _non_threaded_run(self):
        self.testcase.run(self.pg_client)

class Container(object):
    """Container Class
    """
    def __init__(self,
                 name,
                 pg_client):
        """
        Args:
            name(string): Name of the Container
            pg_client(pg_client): The postgres client

        """
        log.debug("Container __init__")
        self.name = name
        self.work_queue = queue.Queue(10000000)
        self.threads = []
        self.pg_client = pg_client
        self.threadLock = threading.RLock()

    def configure_test_discovery(self, discovery_args=None):
        """configure test discovery
        Args:
            discovery_args(dict): dictionary with the string fields start_dir and the subdict keyword_args.
        So far no sanity checks
        """
        self.discovery_args = discovery_args
        if discovery_args is None:
            self.discovery_args = {
                'start_dir': 'fake_tests/',
                'keyword_wargs': {'pattern': 'test_*.py',
                                  'top_level_dir': '../'}}

    def discover_and_queue_tests(self):

        if not self.discovery_args:
            print("discovery not configured, see method configure_test_discovery!")
            return None

        discovery_args = self.discovery_args
        ldr = unittest.TestLoader()
        my_suite = [x for x in ldr.discover(discovery_args['start_dir'],
                                            **discovery_args['keyword_wargs']) if x._tests][0]

        test_cases = []
        for test_case in my_suite.__dict__['_tests'][0].__dict__['_tests']:
            test_cases.append(test_case)

        for test_case in test_cases:
            log.debug("Container {}: adding Test Case {}".format(self.name, str(test_case)))
            self.add_test(test_case)

    def add_test(self, test_case_instance, test_id=None):
        """ Add a test case to the container.
        - create key called "test_id"
        - create initial test data for initializing test into the database
        - create TestCase instance using these data in the testcase
        - enqueue it
        """

        if test_id is None:
            test_id = str(uuid.uuid4())

        initial_data = {'environment': self.name,
                        'status': 'created',
                        'test': str(test_id),
                        'created_at': datetime.datetime.utcnow()}

        test_case = TestCase(initial_data)
        test_case.set_case(test_case_instance)
        test_case.init_db(self.pg_client.connection)
        self.work_queue.put(test_case)

    def nAlive(self):
        """ Get the number of alive threads for this container
        Returns:
            Integer - Number of alive threads
        """
        return len([True for x in self.threads if x.is_alive() or None])

    def report(self):
        """ Utility to give information about threads active and their status
        """
        print('threads:')
        print(threading.enumerate())
        print('Number of  Active Threads')
        print(threading.active_count())
        print('Auto Identifier')
        print(threading.get_ident())
        print('id     alive   daemon')

        for thread in self.threads:
            print(thread.name, thread.is_alive(), thread.isDaemon())

    def run(self):
        """Run tests in the queue
        """
        log.info("starting run for container {}".format(self.name))
        while not self.work_queue.empty():
            testcase = self.work_queue.get()
            thread = ThreadInstance(testcase['test'],
                                    testcase['test'],
                                    self.threadLock,
                                    testcase,
                                    self.pg_client)
            thread.start()
            # Use this to debug:
            # thread._non_threaded_run()
            self.threads.append(thread)

    def join(self, wait=False):
        """ join threads if they are all done
        """
        if not wait:
            if self.nAlive() == 0:
                log.info("joining threaded tests .... ")
                for thread in self.threads:
                    thread.join()
            else:
                log.info('let\'s wait a bit longer!')
        else:
            if self.nAlive() == 0:
                log.info('all threads ready!')
            else:
                log.info('Joining threads, this might take time until they\'re finished')

                for thread in self.threads:
                    thread.join()

    def __del__(self):
        log.debug("Container {} is being deleted".format(self.name))


def main():
    pass


if __name__ == '__main__':
    main()
