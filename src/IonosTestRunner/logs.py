""" Logging facicility
Configure logging:
- The pytest_shell plugins throws lots of unwanted logging messages. Therefore the root logger is set to 'critical' in
order to avoid seeing its messages
-  This is replaced by a module specific logger that writes DEBUG  messages to a logfile, and INFO to the screen
"""

import logging
from logging.config import dictConfig  # noqa
import coloredlogs

from IonosTestRunner import __version__  # noqa

def log_dict():
    logging.basicConfig(level=logging.DEBUG)
    LOGGING_CONFIG = {
        'version': 1,
        'disable_existing_loggers': True,
        'formatters': {
            'standard': {
                'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
            },
            'root-handler-formatter': {
                'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
            },
            'package-handler-formatter': {
                'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
            }
        },
        'handlers': {
            'stream': {
                'level': 'INFO',
                'formatter': 'standard',
                'class': 'logging.StreamHandler',
                'stream': 'ext://sys.stdout',  # Default is stderr
            },
            'root-handler': {
                'level': 'CRITICAL',
                'class': 'logging.FileHandler',
                'filename': '/tmp/logfile',
                'formatter': 'root-handler-formatter'
            },
            'package-handler-testcase_container': {
                'level': 'DEBUG',
                'class': 'logging.FileHandler',
                'filename': '/tmp/logfile',
                'formatter': 'package-handler-formatter'
            },
        },
        'loggers': {
            '': {  # root logger
                'handlers': ['root-handler'],
                'level': 'CRITICAL',
                'propagate': False
            },
            'testcase_container': {  # module
                'handlers': ['package-handler-testcase_container', 'stream'],
                'level': 'DEBUG',
                'propagate': False
            },
            '__main__': {  # if __name__ == '__main__'
                'handlers': ['stream'],
                'level': 'INFO',
                'propagate': False
            },
        }
    }

    coloredlogs.install(level='INFO', logger=logging.getLogger('__main__'))
    return LOGGING_CONFIG

def main():
    pass


if __name__ == '__main__':
    main()
