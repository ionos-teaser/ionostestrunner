#!/bin/bash

function show_table(){
    python manage_testrun_table.py -a show_table  
}

function reset_table(){
    python manage_testrun_table.py -a drop_table
    python manage_testrun_table.py -a create_table
}

function pythonpath(){
    python -c "import sys; import pprint; pprint.pprint(sys.path)"
}
