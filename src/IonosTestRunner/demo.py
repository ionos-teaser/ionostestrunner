#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Demi Run Tests and demonstrate test thread status availability during run
"""

import logging
import shutil
import subprocess as sp
from logging.config import dictConfig
from pprint import pprint as pr  # noqa
from time import sleep

import coloredlogs

from IonosTestRunner.logs import log_dict
from IonosTestRunner.postgres_client.client import Client as PostGresClient
from testcase_container import Container

dictConfig(log_dict())

log = logging.getLogger(__name__)
coloredlogs.install(level='INFO', logger=logging.getLogger('__main__'))

interactive = True

def main():

    log.info("Main Routine for {} about to start".format(__file__))
    shutil.rmtree('logs/', ignore_errors=True)
    pg_client = PostGresClient()

    """ unit test discovery settings """
    discovery_args = {
        'start_dir': 'fake_tests/',
        'keyword_wargs': {'pattern': 'test_*.py',
                          'top_level_dir': '../'}}

    container1 = Container('C1', pg_client)
    container2 = Container('C2', pg_client)

    container1.configure_test_discovery(discovery_args)
    container2.configure_test_discovery(discovery_args)

    container1.discover_and_queue_tests()
    container2.discover_and_queue_tests()

    container1.run()
    container2.run()

    if not interactive:
        while (container1.nAlive() > 0) & (container1.nAlive() > 0):
            sleep(2)
            sp.call('clear', shell=True)
            container1.report()
            sleep(1)
        sp.call('clear', shell=True)
        container1.report()
    else:
        __import__("pdb").set_trace()

    """Things to do interactively """
    """ 1. Thread Status Listing of the threads running in the container """
    # container2.report()
    """ 2. print the number of threads alive in the container """
    log.info(container2.nAlive())
    """ 3. Is the work queue full? """
    container1.work_queue.full()
    """ 4. join thrads before quitting (only when everything is finished) """
    # container2.join()
    """ 5. Prove access to test_run table from dabase during run """
    pg_client.get_df('test_run')
    """ 6. Start again
    """
    #  container2.discover_and_queue_tests()
    # container2.run()
    """ 7. See that the finished threads are not cleaned up
    """
    len(container2.threads)
    """ 8. See that the queue however is empty"""
    qs = container1.work_queue.qsize()

    """ close connection """
    while (container1.nAlive() > 0) & (container2.nAlive() > 0):
        sp.call('clear', shell=True)
        log.info("Alive threads exist, waiting to close db ...")
        sleep(1)
    sp.call('clear', shell=True)

    container1.join()
    container2.join()

    log.info('closing database connection')
    pg_client.close()


if __name__ == '__main__':
        main()
