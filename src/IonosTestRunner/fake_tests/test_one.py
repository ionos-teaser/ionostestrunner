import unittest
import random
import logging
import os
import time
import ntpath

from io import StringIO
from pytest_shell.shell import bash

# TODO: Fixtures
x = 700000
x = (200000, 3000000)  # too long
x = (200000, 300000)  # short
# x = (200000, 8000000)
# x = (200000, 900000)  # long
# x = (20000, 30000)  # very short
# x = (20000, 1000000)

result = 5


def func(x):
    return x + 2


def timing_function(some_function):
    def wrapper(args):
        t1 = time.time()
        some_function(args)
        t2 = time.time()
        print("Time it took to run the function: " + str((t2 - t1)) + "\n")
    return wrapper

# @timing_function
def fib(n):
    """Fibonacci example function

    Args:
      n (int): integer

    Returns:
      int: n-th Fibonacci number
    """

    # logging.getLogger().info('bacon')
    # logging.getLogger().info('bacon')
    # logging.getLogger().info('bacon')
    # logging.getLogger().info('bacon')
    # logging.getLogger().info('bacon')
    # logging.getLogger().info('bacon122222')
    # assert n > 0
    a, b = 1, 1
    for i in range(n-1):
        a, b = b, a+b
    return a


class Test_Fakes(unittest.TestCase):

    def __init__(self, testName, logstream=StringIO()):
        super(Test_Fakes, self).__init__(testName)
        self.logstream = logstream
        self.out = '\nNo Output\n'

    def set_logstream(self, value):
        self.logstream = value

    def write_logs(self, id, methodname):
        ostream = self.logstream
        ostream.write('\ntest four logging in progress')
        ostream.write("\nStarting log for active test case id {}".format(id))
        ostream.write("\nMethod Name: {}".format(self._testMethodName))
        ostream.write("\n---------------------------------\n")
        ostream.write(self.out)
        ostream.write("\n---------------------------------\n")

    def setUp(self):
        pass
        # self.tmpdir = tempfile.mkdtemp()

    def _setHandler(self, test_name):
        log = logging.getLogger()  # root logger
        for hdlr in log.handlers[:]:  # remove all old handlers
            log.removeHandler(hdlr)
        fileh = logging.FileHandler(os.path.join(self.tmpdir, test_name), 'a')
        log.addHandler(fileh)

    def test_one(self):
        fib(random.randint(*x))
        assert func(3) == result

    def test_two(self):
        fib(random.randint(*x))
        assert func(3) == result

    def test_three(self):
        fib(random.randint(*x))
        assert func(3) == result

    def test_four(self):
        fib(random.randint(*x))
        # __import__("pdb").set_trace()
        assert func(3) == result

    def test_something(self):
        abspath, _ = ntpath.split(__file__)
        script_name = os.path.join(abspath, 'dostuff.sh')
        with bash(envvars={'SOMEDIR': '/home/blah'}) as aShell:
            self.out = aShell.run_script(script_name, ['arg1', 'arg2'])

    def tearDown(self):
        self.write_logs(self.id(), self._testMethodName)
