
import logging

logger = logging.getLogger(__name__)


class IonosTestRunnerException(Exception):
    def __init__(self, msg=""):
        logger.exception('An exception was raised: {}'.format(msg))
        Exception.__init__(self, msg)
