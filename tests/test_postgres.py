
import pytest
import unittest

from IonosTestRunner.manage_testrun_table import create_testrun_table
from IonosTestRunner.postgres_client.client import Client as PostGresClient


class Test(unittest.TestCase):
    def setUp(self):
        pass

    @pytest.mark.skip(reason="Test was for development reasons only")
    def test_basic_crud(self):

        pg_client = PostGresClient()

        if 'test_run' in pg_client.get_table_names():
            pg_client.drop_table('test_run')
        create_testrun_table()

        # using reflection https://stackoverflow.com/questions/44193823/get-existing-table-using-sqlalchemy-metadata
        from sqlalchemy import MetaData
        conn = pg_client.connection
        metadata = MetaData(bind=conn, reflect=True)
        test_run = metadata.tables['test_run']

        data_dict = {'status': 'queued'}
        # run a single insert statement:
        stmt = test_run.insert().values(data_dict)
        result = conn.execute(stmt)

        print(pg_client.get_df('test_run'))

        stmt = test_run.insert().values([data_dict, data_dict])
        result = conn.execute(stmt)
        print(type(result))
        # self.insert_meta = self.testrun_table_sqlalch.insert().values(data_dict)

        print(pg_client.get_df('test_run'))

        if 'test_run' in pg_client.get_table_names():
            pg_client.drop_table('test_run')

        pg_client.drop_table('test_run')
        pg_client.close()

    def tearDown(self):
        pass
