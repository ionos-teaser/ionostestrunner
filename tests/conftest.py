# -*- coding: utf-8 -*-
"""
    Dummy conftest.py.

    If you don't know what this is for, just leave it empty.
    Read more about conftest.py under:
    https://pytest.org/latest/plugins.html
"""

# import pytest
pytest_plugins = 'pytester'
