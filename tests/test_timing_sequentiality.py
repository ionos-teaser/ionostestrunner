#!/usr/bin/env python

import sys
sys.path.insert(1, '/media/win-d/myfiles/2019/ionos/ionostestrunner')

import unittest  # noqa

import numpy as np  # noqa
import pandas as pd   # noqa
import pytest   # noqa

from src.IonosTestRunner.postgres_client.client import Client as PostGresClient  # noqa
from src.IonosTestRunner.testcase_container import Container  # noqa


class TestTimingSequentiality(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        pass

    def test_your_code(self):
        discovery_args = {
            'start_dir': 'src/IonosTestRunner/fake_tests/',
            'keyword_wargs': {'pattern': 'test_*.py',
                              'top_level_dir': '../'}}

        discovery_args['start_dir'] = 'src/IonosTestRunner/fake_tests/'
        pg_client = PostGresClient()
        # pg_client.drop_table('test_run')
        container1 = Container('C1', pg_client)
        container1.configure_test_discovery(discovery_args)
        container1.discover_and_queue_tests()
        container1.run()
        container1.join(wait=True)

        df0 = pg_client.get_df('test_run')
        for container in np.unique(df0['environment']):
            df1 = df0[df0['environment'] == container]
            df1.sort_values(by='created_at', inplace=True)
            df1['consecutive_start'] = np.roll(df1['started_at'], -1)
            df1.drop(df1.tail(1).index, inplace=True)
            df1['valid'] = df1['consecutive_start'] > df1['started_at']
            self.assertTrue(np.alltrue(df1['valid']))


    @classmethod
    def tearDownClass(self):
        pass
