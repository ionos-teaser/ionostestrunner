# Setup / Installation


<a id="org4b92067"></a>

## PostgreSQL Database


<a id="org3ea8596"></a>

### Docker-compose Postgres

The directory \`docker/postgres/\` contains a Docker compose file for PostgreSQL.
Run it with

    docker-compose up


<a id="orgfbc4e05"></a>

### Create Credential File

For DB access, a settings file is required in 

    ~/.local/share/IonosTestRunner

Its contents looks like that:

    [LOCALHOST]
    user = christian
    password = passwd
    host = 127.0.0.1
    port = 5432
    db = ionos


<a id="org22a5645"></a>

## Prepare Pip Virtual Environment


<a id="orgfc159c9"></a>

### Create Environment:

    virtualenv ~/.venvs/ionos -p /usr/bin/python3.6


<a id="orgd6b1d71"></a>

### Activate it:

    source ~/.venvs/ionos/bin/activate


<a id="orgd301364"></a>

### Install dependencies:

    pip install -r requirements.txt 


<a id="orgfb10adc"></a>

## Setup PYTHONPATH

The \`src/\` - directory in the project must be made known to your \`PYTHONPATH\`


<a id="org80b921e"></a>

# Getting started

The scripts are called from the \`IonosTestRunner\` directory. 


<a id="orgbc71685"></a>

## Prepare and Manage Table

The  command line tool \`manage<sub>testrun</sub><sub>table.py</sub>\` manages the test<sub>run</sub> table.
Before starting, the \`test<sub>run</sub>\` table needs to be created:

    python manage_testrun_table.py -a create_table  

In reverse, to drop this table again, there is a \`drop<sub>table</sub>\`switch to delete the table again.

    python manage_testrun_table.py -a drop_table

In addition, there is a utility to show the \`test<sub>run</sub>\` table:

    python manage_testrun_table.py -a show_table

This tool can be used to monitor the table status:

    watch -n 2 "python manage_testrun_table.py -a show_table"

For convencience, there is a function \`scripts.sh\` that crates a bash function \`show<sub>table</sub>\` as well
as a second bash function \`reset<sub>table</sub>\` that drops and recreates the table.


<a id="org6d76c48"></a>

## Run the demo

The program \`demo.py\` is also found in \`src/IonosTestRunner\`. It can be run from a terminla as a
simple python script: 

    watch -n 2 "python demo.py"

The program contains a flag \`interactive\` which is set to false by default. 
In this scenario, tests will be 
where various actions can be tried out. If set to \`False\`, then the program polls the number of
active threads and finishes, as soon  all threads except the main thread have exited.
The ese

    import shutil
    import subprocess as sp
    from pprint import pprint as pr  # noqa
    from time import sleep
    
    from IonosTestRunner.postgres_client.client import Client as PostGresClient
    from testcase_container import TestCaseContainer
    
        """ unit test discovery settings """
        discovery_args = {
            'start_dir': 'fake_tests/',
            'keyword_wargs': {'pattern': 'test_*.py',
                              'top_level_dir': '../'}}
    
        container1 = TestCaseContainer('C1', pg_client)
        container2 = TestCaseContainer('C2', pg_client)
    
        container1.configure_test_discovery(discovery_args)
        container2.configure_test_discovery(discovery_args)
    
        container1.discover_and_queue_tests()
        container2.discover_and_queue_tests()
    
        container1.run()
        container2.run()
    
        """Things to do interactively """
        """ 1. Thread Status Listing of the threads running in the container """
        container2.report()
    
        # threads:
        # [<_MainThread(MainThread, started 140573159380800)>, <Thread(Thread-2, started daemon 140572989024000)>, <Heartbeat(Thread-3, started daemon 140572980299520)>, <HistorySavingThread(IPythonHistorySavingThread, started 140572954126080)>, <ParentPollerUnix(Thread-1, started daemon 140572601874176)>]
        # Number of  Active Threads
        # 5
        # Auto Identifier
        # 140573159380800
        # id     alive   daemon
        # 958cba09-d7ed-4484-9ab4-8cd5b63aa981 False False
        # 8ed8f153-958c-46df-b9c3-17ba8f5f156e False False
        # 0b761ef6-3a15-4412-b9e8-15f4ab06d952 False False
        # 30cf7a6e-99a2-41db-8a04-313d75b6f2cf False False
        # 822d0332-73e1-470a-9f77-f0ac1ee5f5e0 False False
    
        """ 2. print the number of threads alive in the container """
        print(container2.nAlive())
        # 2
        """ 3. Is the work queue full? (should not happen) """
        container1.work_queue.full()
        # False
        """ 4. join thrads before quitting (only when everything is finished) """
        container2.join()
        # let's wait a bit longer!
        """ 5. Prove access to test_run table from dabase during run """
        df = pg_client.get_df('test_run')
        df.head()
        Out[87]: 
        id environment                                  test  \
            0  12          C1  f96ee0e2-9f93-4fe8-a307-80149523d816   
        1  23          C1  422a6257-c8a0-4e66-9ee1-53b6153a4edf   
        2   1          C1  0e5c788c-f23f-42ce-b9cf-6ca498de70f2   
        3   6          C2  89ec7bdc-88a5-47d8-88f6-a23fde01664c   
        4  13          C1  fbb16f63-59e7-47b7-803b-07c25cd0114f   
    
        created_at                 started_at  \
        0 2019-11-02 15:47:10.639976 2019-11-02 15:47:11.989598   
        1 2019-11-02 15:51:04.492143 2019-11-02 15:51:06.538714   
        2 2019-11-02 15:34:50.222510 2019-11-02 15:34:50.329986   
        3 2019-11-02 15:34:50.271548 2019-11-02 15:34:50.327857   
        4 2019-11-02 15:47:10.649193 2019-11-02 15:47:12.882952   
    
        finished_at    status  \
        0 2019-11-02 15:47:12.870247  finished   
        1 2019-11-02 15:51:07.366460  finished   
        2 2019-11-02 15:34:51.191230  finished   
        3 2019-11-02 15:34:51.191630  finished   
        4 2019-11-02 15:47:13.703123  finished   
    
        logs  
        0  /media/win-d/myfiles/2019/ionos/asynctester/sr...  
        1  /media/win-d/myfiles/2019/ionos/asynctester/sr...  
        2  /media/win-d/myfiles/2019/ionos/asynctester/sr...  
        3  /media/win-d/myfiles/2019/ionos/asynctester/sr...  
        4  /media/win-d/myfiles/2019/ionos/asynctester/sr...  
        """ 6. Start again
        """
        container2.discover_and_queue_tests()
        container2.run()
        # starting run for container C2
    
        """ 7. See that the finished threads are not cleaned up
        """
        len(container2.threads)
        # 10
        """ 8. See that the queue however is empty after processing finished"""
        container1.work_queue.qsize()
    
        print('closing database connection')
        pg_client.close()


<a id="org7daa7e7"></a>

# Discussion of MustHaves

1.  Run the tests on the local machine.
    -   Only Python's theading module on the local machine is used.

2.  Make sure that all the data required to analyze a test run is available in the database, and can be queried using the software.
    -   The program uses sqlalchemy, and the connection to Postgres can be created in the software, and
        a client object is passed on such that this is possible. The client has a method \`get<sub>df</sub>()\`
        that can query the database. This is demonstrated in the demo under section 5.

3.  Virtual environment safety: make sure that no two tests are run in parallel in a given virtual environment.
    -   Python's threading module is used. Within the container, each test is running in a single
        thread that uses a Python Threading RLock. There is an IPython Notebook called \`Exlplore
        data.ipynb\` that deals with that issue.

4.  Proper integration of the Python test framework of choice into the software: make sure to use the
    test framework features to integrate with your software and collect the data as efficiently as possible.
    -   It uses standard \`unittest\` module's  discovery mechanism. This results in one or more test
        suites. This suite then is then divided into smaller test suites such that each tests becomes
        one single test instance.

5.  Design a simple test runner for bash scripts: while you can use a Python test framework for the Python tests, for the Bash tests you will need to design a simple test runner, making sure that you have an easy way to collect the test data, and that this data is compatible with the data structures.
    -   I am using \`pytest<sub>shell</sub>\` for that purpose.

6.  Good test coverage.
    -   As the tests do not  test any realistic logic, the coverage can automatically be considerered as good. For the code itself, the coverage is bad due to time limitations.. 

7.  Documentation on how to use the software to run tests, deploy the database, etc.
    -   Sofware comes with a README.md at the top level of the project directory.


<a id="org453ad3b"></a>

# Other Limitations

-   Currently, the container has to be restarted once all data in the queue have been processed. 
    It would probably be a good goal to implement a producer-consumer lile pipeline, but this has not
    been tried yet.
-   Coverage is not reported, as on the fake tests that are used here, this does not make much
    sense. In general, tests have been written sparsely due to time constraints
-   Logging


